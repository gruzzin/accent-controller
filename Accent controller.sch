EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Libraries:TP4056 U?
U 1 1 60EDDFFF
P 1500 5750
F 0 "U?" H 1500 6125 50  0000 C CNN
F 1 "TP4056" H 1500 6034 50  0000 C CNN
F 2 "" H 1500 5750 50  0001 C CNN
F 3 "" H 1500 5750 50  0001 C CNN
	1    1500 5750
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:TPS63001 U?
U 1 1 60EE1B26
P 2000 3650
F 0 "U?" H 2000 4317 50  0000 C CNN
F 1 "TPS63001" H 2000 4226 50  0000 C CNN
F 2 "Package_SON:Texas_DRC0010J_ThermalVias" H 2850 3100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tps63000.pdf" H 1700 4200 50  0001 C CNN
	1    2000 3650
	1    0    0    -1  
$EndComp
$Comp
L power:VBUS #PWR?
U 1 1 60EF528A
P 1150 950
F 0 "#PWR?" H 1150 800 50  0001 C CNN
F 1 "VBUS" H 1165 1123 50  0000 C CNN
F 2 "" H 1150 950 50  0001 C CNN
F 3 "" H 1150 950 50  0001 C CNN
	1    1150 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60EF7FE8
P 2900 2950
F 0 "#PWR?" H 2900 2800 50  0001 C CNN
F 1 "+3V3" H 2915 3123 50  0000 C CNN
F 2 "" H 2900 2950 50  0001 C CNN
F 3 "" H 2900 2950 50  0001 C CNN
	1    2900 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:Battery_Cell BT?
U 1 1 60F024A7
P 2500 6150
F 0 "BT?" H 2618 6246 50  0000 L CNN
F 1 "Battery_Cell" H 2618 6155 50  0000 L CNN
F 2 "" V 2500 6210 50  0001 C CNN
F 3 "~" V 2500 6210 50  0001 C CNN
	1    2500 6150
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 60F0957B
P 2500 5950
F 0 "#PWR?" H 2500 5800 50  0001 C CNN
F 1 "+BATT" H 2515 6123 50  0000 C CNN
F 2 "" H 2500 5950 50  0001 C CNN
F 3 "" H 2500 5950 50  0001 C CNN
	1    2500 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60F0A38E
P 2500 6250
F 0 "#PWR?" H 2500 6000 50  0001 C CNN
F 1 "GND" H 2505 6077 50  0000 C CNN
F 2 "" H 2500 6250 50  0001 C CNN
F 3 "" H 2500 6250 50  0001 C CNN
	1    2500 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:L_Small L?
U 1 1 60F0B6EF
P 2000 2850
F 0 "L?" V 2185 2850 50  0000 C CNN
F 1 "2.2u" V 2094 2850 50  0000 C CNN
F 2 "" H 2000 2850 50  0001 C CNN
F 3 "~" H 2000 2850 50  0001 C CNN
	1    2000 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1600 3250 1600 2850
Wire Wire Line
	1600 2850 1900 2850
Wire Wire Line
	2400 3250 2400 2850
Wire Wire Line
	2400 2850 2100 2850
Wire Wire Line
	2400 3450 2450 3450
Wire Wire Line
	2900 3450 2900 2950
Connection ~ 2900 3450
$Comp
L Device:C_Small C?
U 1 1 60F0F1C5
P 2600 3550
F 0 "C?" H 2692 3596 50  0000 L CNN
F 1 "10u" H 2692 3505 50  0000 L CNN
F 2 "" H 2600 3550 50  0001 C CNN
F 3 "~" H 2600 3550 50  0001 C CNN
	1    2600 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60F10480
P 2900 3550
F 0 "C?" H 2992 3596 50  0000 L CNN
F 1 "10u" H 2992 3505 50  0000 L CNN
F 2 "" H 2900 3550 50  0001 C CNN
F 3 "~" H 2900 3550 50  0001 C CNN
	1    2900 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 4250 2000 4250
$Comp
L power:GND #PWR?
U 1 1 60F11D2C
P 2000 4250
F 0 "#PWR?" H 2000 4000 50  0001 C CNN
F 1 "GND" H 2005 4077 50  0000 C CNN
F 2 "" H 2000 4250 50  0001 C CNN
F 3 "" H 2000 4250 50  0001 C CNN
	1    2000 4250
	1    0    0    -1  
$EndComp
Connection ~ 2000 4250
$Comp
L power:GND #PWR?
U 1 1 60F125A1
P 1500 6100
F 0 "#PWR?" H 1500 5850 50  0001 C CNN
F 1 "GND" H 1505 5927 50  0000 C CNN
F 2 "" H 1500 6100 50  0001 C CNN
F 3 "" H 1500 6100 50  0001 C CNN
	1    1500 6100
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J?
U 1 1 60F0771A
P 800 1250
F 0 "J?" H 800 1600 50  0000 C CNN
F 1 "USB_B_Micro" H 857 1626 50  0001 C CNN
F 2 "" H 950 1200 50  0001 C CNN
F 3 "~" H 950 1200 50  0001 C CNN
	1    800  1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	700  1650 800  1650
Text Label 1450 1250 2    50   ~ 0
D+_RAW
Text Label 1450 1350 2    50   ~ 0
D-_RAW
NoConn ~ 1100 1450
Wire Wire Line
	1100 1350 1450 1350
Wire Wire Line
	1100 1250 1450 1250
Text Label 3600 1550 0    50   ~ 0
D-_P
Text Label 3600 1450 0    50   ~ 0
D+_P
Wire Wire Line
	3850 1450 3600 1450
Wire Wire Line
	3600 1550 3850 1550
Text Label 4900 1150 2    50   ~ 0
TXD
Text Label 4900 1250 2    50   ~ 0
RXD
Wire Wire Line
	4900 1250 4650 1250
Wire Wire Line
	4650 1150 4900 1150
Text Label 7350 1450 2    50   ~ 0
TXD
Text Label 7350 1250 2    50   ~ 0
RXD
Wire Wire Line
	7350 1250 7100 1250
Wire Wire Line
	7350 1450 7100 1450
NoConn ~ 4650 1450
NoConn ~ 4650 1950
Wire Wire Line
	4250 950  4150 950 
$Comp
L power:+3V3 #PWR?
U 1 1 60F30965
P 4250 950
F 0 "#PWR?" H 4250 800 50  0001 C CNN
F 1 "+3V3" H 4265 1123 50  0000 C CNN
F 2 "" H 4250 950 50  0001 C CNN
F 3 "" H 4250 950 50  0001 C CNN
	1    4250 950 
	1    0    0    -1  
$EndComp
Connection ~ 4250 950 
Wire Wire Line
	3850 1650 3650 1650
Wire Wire Line
	3650 1650 3650 1750
$Comp
L Device:LED_Small D?
U 1 1 60F350DE
P 3650 2050
F 0 "D?" V 3696 1980 50  0000 R CNN
F 1 "ORG" V 3605 1980 50  0000 R CNN
F 2 "" V 3650 2050 50  0001 C CNN
F 3 "~" V 3650 2050 50  0001 C CNN
	1    3650 2050
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 60F36ADB
P 3650 1850
F 0 "R?" H 3709 1896 50  0000 L CNN
F 1 "680R" H 3709 1805 50  0000 L CNN
F 2 "" H 3650 1850 50  0001 C CNN
F 3 "~" H 3650 1850 50  0001 C CNN
	1    3650 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2150 4250 2150
$Comp
L power:VBUS #PWR?
U 1 1 60F3AB08
P 1050 4900
F 0 "#PWR?" H 1050 4750 50  0001 C CNN
F 1 "VBUS" H 1065 5073 50  0000 C CNN
F 2 "" H 1050 4900 50  0001 C CNN
F 3 "" H 1050 4900 50  0001 C CNN
	1    1050 4900
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60F2C16D
P 6500 950
F 0 "#PWR?" H 6500 800 50  0001 C CNN
F 1 "+3V3" H 6515 1123 50  0000 C CNN
F 2 "" H 6500 950 50  0001 C CNN
F 3 "" H 6500 950 50  0001 C CNN
	1    6500 950 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60F4E7D8
P 6500 3750
F 0 "#PWR?" H 6500 3500 50  0001 C CNN
F 1 "GND" H 6505 3577 50  0000 C CNN
F 2 "" H 6500 3750 50  0001 C CNN
F 3 "" H 6500 3750 50  0001 C CNN
	1    6500 3750
	1    0    0    -1  
$EndComp
Text Label 7600 2850 2    50   ~ 0
EPD_BUSY
Text Label 7600 2950 2    50   ~ 0
EPD_RST
Text Label 7600 3050 2    50   ~ 0
EPD_DC
Text Label 7600 2050 2    50   ~ 0
EPD_CS
Text Label 7600 1850 2    50   ~ 0
EPD_SCK
Text Label 7600 1950 2    50   ~ 0
EPD_DIN
$Comp
L Device:L_Small L?
U 1 1 60F8EC8C
P 5450 5050
F 0 "L?" V 5269 5050 50  0000 C CNN
F 1 "68uH" V 5360 5050 50  0000 C CNN
F 2 "" H 5450 5050 50  0001 C CNN
F 3 "~" H 5450 5050 50  0001 C CNN
	1    5450 5050
	0    1    1    0   
$EndComp
$Comp
L Diode:MBR0530 D?
U 1 1 60F908D9
P 5700 4700
F 0 "D?" V 5700 4620 50  0000 R CNN
F 1 "MBR0530" V 5655 4620 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" H 5700 4525 50  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/MBR0520~MBR0580(SOD123).pdf" H 5700 4700 50  0001 C CNN
	1    5700 4700
	0    -1   -1   0   
$EndComp
$Comp
L Diode:MBR0530 D?
U 1 1 60F91A67
P 6300 5050
F 0 "D?" H 6300 4925 50  0000 C CNN
F 1 "MBR0530" H 6300 4924 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" H 6300 4875 50  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/MBR0520~MBR0580(SOD123).pdf" H 6300 5050 50  0001 C CNN
	1    6300 5050
	-1   0    0    1   
$EndComp
$Comp
L Diode:MBR0530 D?
U 1 1 60F924C8
P 6300 4550
F 0 "D?" H 6300 4675 50  0000 C CNN
F 1 "MBR0530" H 6300 4676 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" H 6300 4375 50  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/MBR0520~MBR0580(SOD123).pdf" H 6300 4550 50  0001 C CNN
	1    6300 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 60F93D3A
P 6050 5400
F 0 "Q?" H 6255 5400 50  0000 L CNN
F 1 "Q_NMOS_GSD" H 6254 5355 50  0001 L CNN
F 2 "" H 6250 5500 50  0001 C CNN
F 3 "~" H 6050 5400 50  0001 C CNN
	1    6050 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60F99B5A
P 6150 4750
F 0 "C?" H 6242 4796 50  0000 L CNN
F 1 "4.7u" H 6242 4705 50  0000 L CNN
F 2 "" H 6150 4750 50  0001 C CNN
F 3 "~" H 6150 4750 50  0001 C CNN
	1    6150 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60F9C435
P 5250 5350
F 0 "C?" H 5342 5396 50  0000 L CNN
F 1 "4.7u" H 5342 5305 50  0000 L CNN
F 2 "" H 5250 5350 50  0001 C CNN
F 3 "~" H 5250 5350 50  0001 C CNN
	1    5250 5350
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60F9FC8B
P 5250 4400
F 0 "#PWR?" H 5250 4250 50  0001 C CNN
F 1 "+3V3" H 5265 4573 50  0000 C CNN
F 2 "" H 5250 4400 50  0001 C CNN
F 3 "" H 5250 4400 50  0001 C CNN
	1    5250 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60FA088F
P 5250 6050
F 0 "#PWR?" H 5250 5800 50  0001 C CNN
F 1 "GND" H 5255 5877 50  0000 C CNN
F 2 "" H 5250 6050 50  0001 C CNN
F 3 "" H 5250 6050 50  0001 C CNN
	1    5250 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 5450 5250 5650
Wire Wire Line
	5250 5250 5250 5050
Connection ~ 5250 5050
Wire Wire Line
	6150 4550 6150 4650
Wire Wire Line
	6150 4850 6150 5050
Connection ~ 6150 5050
Wire Wire Line
	6150 4550 5700 4550
Connection ~ 6150 4550
Wire Wire Line
	5700 4850 5700 5650
Wire Wire Line
	5700 5650 5250 5650
Wire Wire Line
	5250 5050 5350 5050
Wire Wire Line
	5550 5050 6150 5050
Wire Wire Line
	6150 5050 6150 5200
Text Label 6800 4550 2    50   ~ 0
PREVGL
Text Label 6800 5050 2    50   ~ 0
PREVGH
Wire Wire Line
	6800 5050 6450 5050
Wire Wire Line
	6450 4550 6800 4550
Wire Wire Line
	5250 4400 5250 5050
$Comp
L Device:R_Small R?
U 1 1 60FCEB5B
P 6150 5850
F 0 "R?" H 6091 5804 50  0000 R CNN
F 1 "3R" H 6091 5895 50  0000 R CNN
F 2 "" H 6150 5850 50  0001 C CNN
F 3 "~" H 6150 5850 50  0001 C CNN
	1    6150 5850
	-1   0    0    1   
$EndComp
Wire Wire Line
	6150 5600 6150 5750
Wire Wire Line
	6150 5950 5750 5950
Wire Wire Line
	5250 5950 5250 6050
Wire Wire Line
	5250 5650 5250 5950
Connection ~ 5250 5650
Connection ~ 5250 5950
Text Label 5750 5400 0    50   ~ 0
GDR
Wire Wire Line
	5750 5400 5850 5400
$Comp
L Device:R_Small R?
U 1 1 60FDBDCC
P 5750 5750
F 0 "R?" H 5691 5704 50  0000 R CNN
F 1 "10k" H 5691 5795 50  0000 R CNN
F 2 "" H 5750 5750 50  0001 C CNN
F 3 "~" H 5750 5750 50  0001 C CNN
	1    5750 5750
	-1   0    0    1   
$EndComp
Wire Wire Line
	5750 5400 5750 5650
Wire Wire Line
	5750 5850 5750 5950
Connection ~ 5750 5950
Wire Wire Line
	5750 5950 5250 5950
Wire Wire Line
	7600 1850 7100 1850
Wire Wire Line
	7600 1950 7100 1950
Wire Wire Line
	7600 2050 7100 2050
Wire Wire Line
	7600 2850 7100 2850
Wire Wire Line
	7600 3050 7100 3050
NoConn ~ 5900 1350
NoConn ~ 5900 1450
NoConn ~ 5900 2350
NoConn ~ 5900 2450
NoConn ~ 5900 2550
NoConn ~ 5900 2650
NoConn ~ 5900 2750
NoConn ~ 5900 2850
Text Label 6800 5600 2    50   ~ 0
RESE
Wire Wire Line
	6800 5600 6150 5600
Connection ~ 6150 5600
Wire Notes Line
	7050 4100 7050 6450
Wire Notes Line
	7050 6450 5050 6450
Wire Notes Line
	5050 6450 5050 4100
Wire Notes Line
	5050 4100 7050 4100
Text Notes 5100 6400 0    50   ~ 0
Screen DC-DC supply
Text Notes 6100 6350 0    50   ~ 0
MOSFET 30V Vds, 1A Id\nSi1308, IRLML0030
$Comp
L power:+BATT #PWR?
U 1 1 60F9E805
P 750 2950
F 0 "#PWR?" H 750 2800 50  0001 C CNN
F 1 "+BATT" H 765 3123 50  0000 C CNN
F 2 "" H 750 2950 50  0001 C CNN
F 3 "" H 750 2950 50  0001 C CNN
	1    750  2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  2950 750  3450
Wire Wire Line
	750  3450 1200 3450
$Comp
L Device:C_Small C?
U 1 1 60FA735D
P 750 3550
F 0 "C?" H 842 3596 50  0000 L CNN
F 1 "10u" H 842 3505 50  0000 L CNN
F 2 "" H 750 3550 50  0001 C CNN
F 3 "~" H 750 3550 50  0001 C CNN
	1    750  3550
	1    0    0    -1  
$EndComp
Connection ~ 750  3450
$Comp
L Device:R_Small R?
U 1 1 60FA7E44
P 1200 3600
F 0 "R?" H 1259 3646 50  0000 L CNN
F 1 "100R" H 1259 3555 50  0000 L CNN
F 2 "" H 1200 3600 50  0001 C CNN
F 3 "~" H 1200 3600 50  0001 C CNN
	1    1200 3600
	1    0    0    -1  
$EndComp
Connection ~ 1200 3450
Wire Wire Line
	1200 3450 1600 3450
$Comp
L Device:C_Small C?
U 1 1 60FA8AED
P 1200 3850
F 0 "C?" H 1292 3896 50  0000 L CNN
F 1 "100n" H 1292 3805 50  0000 L CNN
F 2 "" H 1200 3850 50  0001 C CNN
F 3 "~" H 1200 3850 50  0001 C CNN
	1    1200 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 3450 1200 3500
Wire Wire Line
	1200 3700 1200 3750
Wire Wire Line
	1200 3950 1200 4250
Wire Wire Line
	1200 4250 1900 4250
Connection ~ 1900 4250
Wire Wire Line
	750  3650 750  4250
Wire Wire Line
	750  4250 1200 4250
Connection ~ 1200 4250
Wire Wire Line
	1200 3750 1600 3750
Wire Wire Line
	1600 3750 1600 3850
Connection ~ 1200 3750
Wire Wire Line
	1600 3750 1600 3650
Connection ~ 1600 3750
Connection ~ 1600 3650
Wire Wire Line
	1600 3650 1600 3550
Wire Wire Line
	2450 3450 2450 3650
Connection ~ 2450 3450
Wire Wire Line
	2450 3450 2600 3450
Wire Wire Line
	2450 3650 2400 3650
Connection ~ 2600 3450
Wire Wire Line
	2600 3450 2900 3450
Connection ~ 2600 3650
Wire Wire Line
	2600 3650 2900 3650
Wire Wire Line
	2600 3650 2600 4250
Wire Wire Line
	2600 4250 2000 4250
$Comp
L Device:R_Small R?
U 1 1 60FCF5E3
P 850 5800
F 0 "R?" H 700 5850 50  0000 L CNN
F 1 "1.2k" H 650 5750 50  0000 L CNN
F 2 "" H 850 5800 50  0001 C CNN
F 3 "~" H 850 5800 50  0001 C CNN
	1    850  5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 5700 850  5700
Wire Wire Line
	850  5900 850  6100
Wire Wire Line
	850  6100 950  6100
Connection ~ 1500 6100
Wire Wire Line
	1100 5850 950  5850
Wire Wire Line
	950  5850 950  6100
Connection ~ 950  6100
Wire Wire Line
	950  6100 1500 6100
Wire Wire Line
	1100 5600 950  5600
Wire Wire Line
	950  5600 950  5850
Connection ~ 950  5850
Wire Wire Line
	1050 5950 1100 5950
$Comp
L Device:C_Small C?
U 1 1 60FF3A89
P 750 5350
F 0 "C?" H 842 5396 50  0000 L CNN
F 1 "100n" H 842 5305 50  0000 L CNN
F 2 "" H 750 5350 50  0001 C CNN
F 3 "~" H 750 5350 50  0001 C CNN
	1    750  5350
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60FF45F5
P 3550 1200
F 0 "C?" H 3642 1246 50  0000 L CNN
F 1 "100n" H 3642 1155 50  0000 L CNN
F 2 "" H 3550 1200 50  0001 C CNN
F 3 "~" H 3550 1200 50  0001 C CNN
	1    3550 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 5700 2100 5700
Wire Wire Line
	2300 5700 2300 5850
Wire Wire Line
	2300 5850 1900 5850
Wire Wire Line
	1900 5600 1900 5100
Wire Wire Line
	1050 5100 1050 5950
Wire Wire Line
	1050 5100 1900 5100
$Comp
L Device:LED_Dual_AACC D?
U 1 1 60FFB2F6
P 2200 5400
F 0 "D?" V 2154 5688 50  0000 L CNN
F 1 "LED_Dual_AACC" V 2245 5688 50  0000 L CNN
F 2 "" H 2230 5400 50  0001 C CNN
F 3 "~" H 2230 5400 50  0001 C CNN
	1    2200 5400
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 6102DEB6
P 1500 4900
F 0 "R?" V 1300 4800 50  0000 L CNN
F 1 "680R" V 1400 4800 50  0000 L CNN
F 2 "" H 1500 4900 50  0001 C CNN
F 3 "~" H 1500 4900 50  0001 C CNN
	1    1500 4900
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 61030FE3
P 1800 5000
F 0 "R?" V 1550 4950 50  0000 L CNN
F 1 "680R" V 1650 4900 50  0000 L CNN
F 2 "" H 1800 5000 50  0001 C CNN
F 3 "~" H 1800 5000 50  0001 C CNN
	1    1800 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	2100 5100 2100 5000
Wire Wire Line
	2100 5000 1900 5000
Wire Wire Line
	2300 5100 2300 4900
Wire Wire Line
	2300 4900 1600 4900
Wire Wire Line
	1700 5000 1050 5000
Wire Wire Line
	1050 5000 1050 5100
Connection ~ 1050 5100
Wire Wire Line
	1400 4900 1050 4900
Wire Wire Line
	1050 4900 1050 5000
Connection ~ 1050 4900
Connection ~ 1050 5000
Wire Wire Line
	1900 5950 2500 5950
Connection ~ 2500 5950
Wire Wire Line
	7550 2950 7100 2950
$Comp
L RF_Module:ESP32-WROOM-32D U?
U 1 1 60EDB8A6
P 6500 2350
F 0 "U?" H 6500 2800 50  0000 C CNN
F 1 "ESP32-WROOM-32D" H 6500 3850 50  0001 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 6500 850 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32d_esp32-wroom-32u_datasheet_en.pdf" H 6200 2400 50  0001 C CNN
	1    6500 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 60F4C032
P 6000 950
F 0 "R?" V 5804 950 50  0000 C CNN
F 1 "10k" V 5895 950 50  0000 C CNN
F 2 "" H 6000 950 50  0001 C CNN
F 3 "~" H 6000 950 50  0001 C CNN
	1    6000 950 
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 60F4E0A5
P 7000 950
F 0 "R?" V 6804 950 50  0000 C CNN
F 1 "10k" V 6895 950 50  0000 C CNN
F 2 "" H 7000 950 50  0001 C CNN
F 3 "~" H 7000 950 50  0001 C CNN
	1    7000 950 
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 950  6500 950 
Connection ~ 6500 950 
Wire Wire Line
	6500 950  6900 950 
Wire Wire Line
	5900 950  5900 1150
Wire Wire Line
	7100 950  7100 1150
Connection ~ 5900 1150
$Comp
L Device:C_Small C?
U 1 1 60F73C25
P 5550 1350
F 0 "C?" H 5642 1396 50  0000 L CNN
F 1 "100n" H 5642 1305 50  0000 L CNN
F 2 "" H 5550 1350 50  0001 C CNN
F 3 "~" H 5550 1350 50  0001 C CNN
	1    5550 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 1550 5250 1550
$Comp
L power:GND #PWR?
U 1 1 60F86866
P 4250 2150
F 0 "#PWR?" H 4250 1900 50  0001 C CNN
F 1 "GND" H 4255 1977 50  0000 C CNN
F 2 "" H 4250 2150 50  0001 C CNN
F 3 "" H 4250 2150 50  0001 C CNN
	1    4250 2150
	1    0    0    -1  
$EndComp
Connection ~ 4250 2150
$Comp
L power:GND #PWR?
U 1 1 60F86F90
P 5550 1550
F 0 "#PWR?" H 5550 1300 50  0001 C CNN
F 1 "GND" H 5555 1377 50  0000 C CNN
F 2 "" H 5550 1550 50  0001 C CNN
F 3 "" H 5550 1550 50  0001 C CNN
	1    5550 1550
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push PROG
U 1 1 60F87672
P 7800 1400
F 0 "PROG" V 7800 1352 50  0000 R CNN
F 1 "SW_Push" V 7755 1352 50  0001 R CNN
F 2 "" H 7800 1600 50  0001 C CNN
F 3 "~" H 7800 1600 50  0001 C CNN
	1    7800 1400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60F89222
P 7800 1600
F 0 "#PWR?" H 7800 1350 50  0001 C CNN
F 1 "GND" H 7805 1427 50  0000 C CNN
F 2 "" H 7800 1600 50  0001 C CNN
F 3 "" H 7800 1600 50  0001 C CNN
	1    7800 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 1200 7800 1150
Wire Wire Line
	7800 1150 7100 1150
Connection ~ 7100 1150
NoConn ~ 7100 1350
NoConn ~ 7100 1550
NoConn ~ 7100 1650
NoConn ~ 7100 1750
NoConn ~ 7100 2150
NoConn ~ 7100 2250
NoConn ~ 7100 2350
NoConn ~ 7100 2450
NoConn ~ 7100 2550
NoConn ~ 7100 2650
NoConn ~ 7100 2750
NoConn ~ 7100 3150
NoConn ~ 7100 3250
NoConn ~ 7100 3350
NoConn ~ 7100 3450
$Comp
L Switch:SW_Push RST
U 1 1 60F754F3
P 5250 1350
F 0 "RST" V 5250 1302 50  0000 R CNN
F 1 "SW_Push" V 5205 1302 50  0001 R CNN
F 2 "" H 5250 1550 50  0001 C CNN
F 3 "~" H 5250 1550 50  0001 C CNN
	1    5250 1350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5250 1150 5550 1150
Wire Wire Line
	5550 1250 5550 1150
Connection ~ 5550 1150
Wire Wire Line
	5550 1150 5900 1150
Wire Wire Line
	5550 1450 5550 1550
Connection ~ 5550 1550
Wire Wire Line
	1100 1050 1150 1050
Wire Wire Line
	1150 1050 1150 950 
Connection ~ 800  1650
$Comp
L power:GND #PWR?
U 1 1 6106D270
P 800 1650
F 0 "#PWR?" H 800 1400 50  0001 C CNN
F 1 "GND" H 805 1477 50  0000 C CNN
F 2 "" H 800 1650 50  0001 C CNN
F 3 "" H 800 1650 50  0001 C CNN
	1    800  1650
	1    0    0    -1  
$EndComp
$Comp
L Interface_USB:CH340E U?
U 1 1 60F122DA
P 4250 1550
F 0 "U?" H 4250 1750 50  0000 C CNN
F 1 "CH340E" H 4250 770 50  0001 C CNN
F 2 "Package_SO:MSOP-10_3x3mm_P0.5mm" H 4300 1000 50  0001 L CNN
F 3 "https://www.mpja.com/download/35227cpdata.pdf" H 3900 2350 50  0001 C CNN
	1    4250 1550
	1    0    0    -1  
$EndComp
$Comp
L power:VBUS #PWR?
U 1 1 6106DABA
P 2550 950
F 0 "#PWR?" H 2550 800 50  0001 C CNN
F 1 "VBUS" H 2565 1123 50  0000 C CNN
F 2 "" H 2550 950 50  0001 C CNN
F 3 "" H 2550 950 50  0001 C CNN
	1    2550 950 
	1    0    0    -1  
$EndComp
$Comp
L Power_Protection:USBLC6-2P6 U?
U 1 1 60ED7007
P 2550 1350
F 0 "U?" H 2600 1000 50  0000 L CNN
F 1 "USBLC6-2P6" V 2595 1794 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-666" H 2550 850 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/usblc6-2.pdf" H 2750 1700 50  0001 C CNN
	1    2550 1350
	1    0    0    -1  
$EndComp
Text Label 1800 1250 0    50   ~ 0
D+_RAW
Text Label 3300 1250 2    50   ~ 0
D-_RAW
Wire Wire Line
	3300 1250 2950 1250
Wire Wire Line
	2150 1250 1800 1250
Text Label 1800 1450 0    50   ~ 0
D+_P
Text Label 3300 1450 2    50   ~ 0
D-_P
Wire Wire Line
	3300 1450 2950 1450
Wire Wire Line
	2150 1450 1800 1450
$Comp
L power:GND #PWR?
U 1 1 60EFEBF1
P 2550 1750
F 0 "#PWR?" H 2550 1500 50  0001 C CNN
F 1 "GND" H 2555 1577 50  0000 C CNN
F 2 "" H 2550 1750 50  0001 C CNN
F 3 "" H 2550 1750 50  0001 C CNN
	1    2550 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 5100 750  5100
Wire Wire Line
	750  5100 750  5250
Wire Wire Line
	750  5450 750  6100
Wire Wire Line
	750  6100 850  6100
Connection ~ 850  6100
Wire Wire Line
	4150 950  3550 950 
Wire Wire Line
	3550 950  3550 1100
Connection ~ 4150 950 
Wire Wire Line
	3550 1300 3550 2150
Wire Wire Line
	3550 2150 3650 2150
Connection ~ 3650 2150
Wire Notes Line
	550  650  550  2450
Wire Notes Line
	550  2450 4950 2450
Wire Notes Line
	4950 2450 4950 650 
Wire Notes Line
	4950 650  550  650 
Text Notes 600  2400 0    50   ~ 0
USB socket, ESD protection, USB2Serial
Wire Notes Line
	550  2550 3250 2550
Wire Notes Line
	3250 2550 3250 4550
Wire Notes Line
	3250 4550 550  4550
Wire Notes Line
	550  4550 550  2550
Text Notes 550  4550 0    50   ~ 0
Main DC-DC supply
Wire Notes Line
	550  6500 550  4650
Text Notes 600  6450 0    50   ~ 0
Battery charger
Wire Notes Line
	8100 650  8100 4000
Wire Notes Line
	8100 4000 5050 4000
Wire Notes Line
	5050 4000 5050 650 
Wire Notes Line
	5050 650  8100 650 
Text Notes 5100 3950 0    50   ~ 0
ESP32S
Wire Notes Line
	3250 4650 3250 6500
Wire Notes Line
	550  6500 3250 6500
Wire Notes Line
	3250 4650 550  4650
Text Notes 8250 4850 0    50   ~ 0
Display connector & decoupling
Wire Notes Line
	8200 4850 8200 650 
Wire Notes Line
	10800 4850 8200 4850
Wire Notes Line
	10800 650  10800 4850
Wire Notes Line
	8200 650  10800 650 
Wire Wire Line
	8950 3400 10150 3400
Connection ~ 8950 3400
Wire Wire Line
	8950 1600 9950 1600
Wire Wire Line
	8950 1600 8950 3400
Wire Wire Line
	8400 3950 8400 4300
Wire Wire Line
	8700 3950 8700 4300
Wire Wire Line
	9000 3950 9000 4300
Wire Wire Line
	9300 3950 9300 4300
Wire Wire Line
	9600 3950 9600 4300
Wire Wire Line
	9900 3950 9900 4300
Wire Wire Line
	10200 3950 10200 4300
Wire Wire Line
	10500 3950 10500 4300
Text Label 10500 3950 3    50   ~ 0
VCOM
Text Label 10200 3950 3    50   ~ 0
PREVGL
Text Label 9900 3950 3    50   ~ 0
VSL
Text Label 9600 3950 3    50   ~ 0
PREVGH
Text Label 9300 3950 3    50   ~ 0
VPP
Text Label 9000 3950 3    50   ~ 0
VDD
Text Label 8700 3950 3    50   ~ 0
VGH
Text Label 8400 3950 3    50   ~ 0
VGL
$Comp
L power:GND #PWR?
U 1 1 60F50303
P 9600 4500
F 0 "#PWR?" H 9600 4250 50  0001 C CNN
F 1 "GND" H 9605 4327 50  0000 C CNN
F 2 "" H 9600 4500 50  0001 C CNN
F 3 "" H 9600 4500 50  0001 C CNN
	1    9600 4500
	1    0    0    -1  
$EndComp
Connection ~ 10150 3400
$Comp
L power:GND #PWR?
U 1 1 60F4F2B0
P 10150 3400
F 0 "#PWR?" H 10150 3150 50  0001 C CNN
F 1 "GND" H 10155 3227 50  0000 C CNN
F 2 "" H 10150 3400 50  0001 C CNN
F 3 "" H 10150 3400 50  0001 C CNN
	1    10150 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 4500 10500 4500
Connection ~ 10200 4500
Wire Wire Line
	9900 4500 10200 4500
Connection ~ 9900 4500
Wire Wire Line
	9600 4500 9900 4500
Connection ~ 9600 4500
Wire Wire Line
	9300 4500 9600 4500
Connection ~ 9300 4500
Wire Wire Line
	9000 4500 9300 4500
Connection ~ 9000 4500
Wire Wire Line
	8700 4500 9000 4500
Connection ~ 8700 4500
Wire Wire Line
	8400 4500 8700 4500
$Comp
L Device:C_Small C?
U 1 1 60F4ABC4
P 10500 4400
F 0 "C?" H 10592 4446 50  0000 L CNN
F 1 "1u" H 10592 4355 50  0000 L CNN
F 2 "" H 10500 4400 50  0001 C CNN
F 3 "~" H 10500 4400 50  0001 C CNN
	1    10500 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60F4AA36
P 10200 4400
F 0 "C?" H 10292 4446 50  0000 L CNN
F 1 "1u" H 10292 4355 50  0000 L CNN
F 2 "" H 10200 4400 50  0001 C CNN
F 3 "~" H 10200 4400 50  0001 C CNN
	1    10200 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60F4A663
P 9900 4400
F 0 "C?" H 9992 4446 50  0000 L CNN
F 1 "1u" H 9992 4355 50  0000 L CNN
F 2 "" H 9900 4400 50  0001 C CNN
F 3 "~" H 9900 4400 50  0001 C CNN
	1    9900 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60F4A46D
P 9600 4400
F 0 "C?" H 9692 4446 50  0000 L CNN
F 1 "1u" H 9692 4355 50  0000 L CNN
F 2 "" H 9600 4400 50  0001 C CNN
F 3 "~" H 9600 4400 50  0001 C CNN
	1    9600 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60F4A1DF
P 9300 4400
F 0 "C?" H 9392 4446 50  0000 L CNN
F 1 "1u" H 9392 4355 50  0000 L CNN
F 2 "" H 9300 4400 50  0001 C CNN
F 3 "~" H 9300 4400 50  0001 C CNN
	1    9300 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60F49CCA
P 9000 4400
F 0 "C?" H 9092 4446 50  0000 L CNN
F 1 "1u" H 9092 4355 50  0000 L CNN
F 2 "" H 9000 4400 50  0001 C CNN
F 3 "~" H 9000 4400 50  0001 C CNN
	1    9000 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60F4984D
P 8700 4400
F 0 "C?" H 8792 4446 50  0000 L CNN
F 1 "1u" H 8792 4355 50  0000 L CNN
F 2 "" H 8700 4400 50  0001 C CNN
F 3 "~" H 8700 4400 50  0001 C CNN
	1    8700 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60F48E7E
P 8400 4400
F 0 "C?" H 8492 4446 50  0000 L CNN
F 1 "1u" H 8492 4355 50  0000 L CNN
F 2 "" H 8400 4400 50  0001 C CNN
F 3 "~" H 8400 4400 50  0001 C CNN
	1    8400 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 3200 9500 3200
Wire Wire Line
	9500 3100 9950 3100
Wire Wire Line
	9950 3000 9500 3000
Wire Wire Line
	9500 2900 9950 2900
Wire Wire Line
	9950 2800 9500 2800
Wire Wire Line
	9500 2700 9950 2700
Wire Wire Line
	9950 2600 9500 2600
Wire Wire Line
	9500 2200 9950 2200
Wire Wire Line
	9950 2100 9500 2100
Wire Wire Line
	9500 2000 9950 2000
Wire Wire Line
	9950 1900 9500 1900
Wire Wire Line
	9500 1800 9950 1800
Wire Wire Line
	9950 1700 9500 1700
Wire Wire Line
	9950 1300 9500 1300
Wire Wire Line
	9500 1200 9950 1200
Wire Wire Line
	9950 1100 9500 1100
Wire Wire Line
	9500 1000 9950 1000
Text Label 9500 1300 0    50   ~ 0
VGH
Text Label 9500 1200 0    50   ~ 0
VGL
Text Label 9500 2600 0    50   ~ 0
VDD
Text Label 9500 2700 0    50   ~ 0
VPP
Text Label 9500 2800 0    50   ~ 0
VSH
Text Label 9500 3000 0    50   ~ 0
VSL
Text Label 9500 3200 0    50   ~ 0
VCOM
Connection ~ 8750 2500
Wire Wire Line
	8750 3400 8950 3400
Wire Wire Line
	8750 2500 8750 3400
Text Label 9500 3100 0    50   ~ 0
PREVGL
Text Label 9500 2900 0    50   ~ 0
PREVGH
Connection ~ 8750 2300
$Comp
L Device:C_Small C?
U 1 1 60F2DADF
P 8750 2400
F 0 "C?" H 8842 2446 50  0000 L CNN
F 1 "1u" H 8842 2355 50  0000 L CNN
F 2 "" H 8750 2400 50  0001 C CNN
F 3 "~" H 8750 2400 50  0001 C CNN
	1    8750 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 2500 8750 2500
$Comp
L power:+3V3 #PWR?
U 1 1 60F2A5A1
P 8750 900
F 0 "#PWR?" H 8750 750 50  0001 C CNN
F 1 "+3V3" H 8765 1073 50  0000 C CNN
F 2 "" H 8750 900 50  0001 C CNN
F 3 "" H 8750 900 50  0001 C CNN
	1    8750 900 
	1    0    0    -1  
$EndComp
Connection ~ 9950 2300
Wire Wire Line
	8750 2300 8750 900 
Wire Wire Line
	9950 2300 8750 2300
Wire Wire Line
	9950 2400 9950 2300
Text Label 9500 2200 0    50   ~ 0
EPD_DIN
Text Label 9500 2100 0    50   ~ 0
EPD_SCK
Text Label 9500 2000 0    50   ~ 0
EPD_CS
Text Label 9500 1900 0    50   ~ 0
EPD_DC
Text Label 9500 1800 0    50   ~ 0
EPD_RST
Text Label 9500 1700 0    50   ~ 0
EPD_BUSY
Text Label 9500 1600 0    50   ~ 0
BS
NoConn ~ 9950 1500
NoConn ~ 9950 1400
Text Label 9500 1100 0    50   ~ 0
RESE
Text Label 9500 1000 0    50   ~ 0
GDR
NoConn ~ 9950 900 
$Comp
L Connector_Generic_MountingPin:Conn_01x24_MountingPin J?
U 1 1 60EE3713
P 10150 2000
F 0 "J?" H 10238 1914 50  0000 L CNN
F 1 "EDP" H 10238 1823 50  0000 L CNN
F 2 "" H 10150 2000 50  0001 C CNN
F 3 "~" H 10150 2000 50  0001 C CNN
	1    10150 2000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
